import * as React from 'react';
import DevQuizComponent from './components/DevQuizComponent';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { QuestionService } from './services/QuestionService';
import { UserService } from './services/UserService';
import './App.css';

// const logo = require('./logo.svg');

class App extends React.Component {
  
  private questionService: QuestionService;
  private userService: UserService;

  constructor() {
    super();
    this.questionService = new QuestionService();
    this.userService = new UserService();
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>{'{'}Welcome to the</h2>
          <h1>[Dev] Quiz{'}'}</h1>
        </div>
        <div className="App-content" >
          <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)} >
            <DevQuizComponent questionService={this.questionService} userService={this.userService} />
          </MuiThemeProvider>
        </div>
      </div>
    );
  }

}

export default App;
