import { User, UserResults } from '../model/User';
import { status } from './fetch-util';

export class UserService {
  private static readonly apiUrl = process.env.REACT_APP_API_URL;
  private static readonly createEndpoint = `${UserService.apiUrl}/user/new`;
  private static readonly resultEndpoint = `${UserService.apiUrl}/user/result`;

  private currentUser: User;

  getCurrentUser(): User {
    return this.currentUser;
  }

  createUser(name: string): Promise<User> {
    return fetch(UserService.createEndpoint, {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      credentials: 'include',
      body: JSON.stringify({ name: name })
    })
      .then(status)
      .then(r => r.json() as Promise<User>)
      .then(user => {
        this.setUser(user);
        return user;
      });
  }
  
  getResults(uuid: string): Promise<UserResults> {
    return fetch(`${UserService.resultEndpoint}/uuid`)
      .then(status)
      .then(r => r.json() as Promise<UserResults>)
      .then(user => {
        this.setUser(user);
        return user;
      });
  }

  private setUser(user: User) {
    this.currentUser = user;
  }

}