import { QuestionResponse, Question,  AnsweredQuestion, NewQuestion } from '../model/Question';
import { status } from './fetch-util';

export class QuestionService {
    private static readonly apiUrl = process.env.REACT_APP_API_URL;
    private static readonly questionsEndpoint = `${QuestionService.apiUrl}/question`;
    private static readonly answerEndpoint = `${QuestionService.questionsEndpoint}/answer`;

    getQuestion(): Promise<Question> {
        return fetch(QuestionService.questionsEndpoint)  
        .then(status)  
        .then(r => r.json() as Promise<Question>);
    }

    answerQuestion(response: QuestionResponse): Promise<AnsweredQuestion> {
        return fetch(QuestionService.answerEndpoint, {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            credentials: 'include',
            body: JSON.stringify(response)
        })  
        .then(status)  
        .then(r => r.json() as Promise<AnsweredQuestion>);
    }

    createQuestion(question: NewQuestion) {
        return fetch(QuestionService.questionsEndpoint, {
            method: 'put',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
              },
            credentials: 'include',
            body: JSON.stringify(question)
        })  
        .then(status)  
        .then(r => r.json() as Promise<Question>);
    }

}