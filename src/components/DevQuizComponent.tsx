
import * as React from 'react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Question, AnsweredQuestion, NewQuestion } from '../model/Question';
import { QuestionComponent } from './QuestionComponent';
import { AnsweredQuestionsComponent } from './AnsweredQuestionsComponent';
import { QuestionService } from '../services/QuestionService';
import UserNamingDialog from './UserNamingDialog';
import { UserService } from '../services/UserService';
import { User, UserResults } from '../model/User';
import './QuestionComponent.css';
import QuestionCreateDialog from './QuestionCreateDialog';
import { Snackbar } from 'material-ui';

// tslint:disable-next-line:no-any
class DevQuizComponent extends React.Component<DevQuizProps, DevQuizState> {

  constructor(props?: DevQuizProps) {
    super(props ? props : new DevQuizProps());
    this.state = new DevQuizState();
  }

  componentDidMount() {
    this.nextQuestion();
  }

  render() {
    return (
      <div>
        {this.getQuestionComponent(this.state.currentQuestion, this.state.user)}
        <AnsweredQuestionsComponent questions={this.state.previousQuestions} />
        {this.getUserNamingDialog(this.state.user)}
        {this.getAddQuestionDialog(this.state.showAddQuestionDialog)}
        {this.getAddButton(this.state.canAddQuestion)}
        <Snackbar
          open={this.state.snackBarOpen}
          message="Question added"
          autoHideDuration={4000}
          onRequestClose={() => this.setState({snackBarOpen: false})}
        />
      </div>
    );
  }

  private getQuestionComponent(question: Question, user: User) {
    return question && user ? (
      <QuestionComponent
        id={question.id}
        description={question.description}
        onAnswer={(answer: boolean) => this.handleAnswer(answer)}
      />
    ) : null;
  }

  private getUserNamingDialog(user: User) {
    return user ? null
      : (
        <UserNamingDialog onNameSubmit={(name: string) => this.setUsername(name)} />
      );
  }

  private getAddButton(canAddQuestion: boolean) {
    return canAddQuestion ? (
      <FloatingActionButton className="FloatingAddButton" onClick={() => this.setState({showAddQuestionDialog: true})} >
        <ContentAdd />
      </FloatingActionButton>
    ) : null;
  }

  private getAddQuestionDialog(showAddQuestion: boolean) {
    return showAddQuestion ? (
      <QuestionCreateDialog onSubmit={(q: NewQuestion) => this.handleNewQuestion(q)} />
    ) : null;
  }

  private handleAnswer(answer: boolean) {
    const answeredQuestion = this.questionService.answerQuestion({
      id: this.state.currentQuestion.id,
      response: answer,
      userUUID: this.state.user ? this.state.user.uuid.toString() : ''
    }).then(q => {
      this.saveAnswer(q);
      this.nextQuestion();
    });
  }

  private handleNewQuestion(question: NewQuestion) {
    question.creator = this.state.user.uuid.toString();
    this.setState({showAddQuestionDialog: false});
    this.props.questionService.createQuestion(question)
      .then(() => this.setState({snackBarOpen: true}));
  }

  private saveAnswer(questionResult: AnsweredQuestion) {
    const previousQuestions = this.state.previousQuestions;
    previousQuestions.unshift(questionResult);

    this.setState({ 
      previousQuestions: previousQuestions,
      canAddQuestion: questionResult.canAddQuestion
    });
  }

  private nextQuestion() {
    this.questionService.getQuestion()
      .then(q => this.setState({ currentQuestion: q }));
  }

  private setUsername(name: string) {
    this.props.userService.createUser(name)
      .then(user => {
        this.setState({ user: user });
      });
  }

  private get questionService(): QuestionService {
    return this.props.questionService;
  }

}

class DevQuizProps {
  questionService: QuestionService;
  userService: UserService;
}

class DevQuizState {
  currentQuestion: Question;
  previousQuestions: AnsweredQuestion[] = [];
  user: UserResults;
  canAddQuestion = false;
  showAddQuestionDialog = false;
  snackBarOpen = false;
}

export default DevQuizComponent;