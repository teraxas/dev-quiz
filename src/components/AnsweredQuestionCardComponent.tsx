
import * as React from 'react';
import { Card, CardText, CardHeader } from 'material-ui';
import { Question, AnsweredQuestion } from '../model/Question';
import './QuestionComponent.css';

export class AnsweredQuestionCardComponent extends React.Component<
  { question: AnsweredQuestion },
  { expanded: boolean }
  > {

  constructor(props: { question: AnsweredQuestion }) {
    super(props);
    this.state = { expanded: false };
  }

  render() {
    return (
      <Card
        className={this.getCardClass(this.question.result)}
        expanded={this.state.expanded}
        onExpandChange={() => this.handleExpandCard()}
      >
        <CardHeader
          title={this.question.description}
          actAsExpander={true}
          showExpandableButton={true}
        />
        <CardText expandable={true} >
          {this.question.explanation}
        </CardText>
      </Card>
    );
  }

  get question(): AnsweredQuestion {
    return this.props.question;
  }

  private getCardClass(correct?: boolean): string {
    return `QuestionCard Answered ${correct ? 'Correct' : 'Incorrect'} `;
  }

  private handleExpandCard() {
    this.setState({ expanded: !this.state.expanded });
  }

}

export default AnsweredQuestionCardComponent;