
import * as React from 'react';
import { AnsweredQuestion } from '../model/Question';
import { AnsweredQuestionCardComponent } from './AnsweredQuestionCardComponent';
import './QuestionComponent.css';

// tslint:disable-next-line:no-any
export class AnsweredQuestionsComponent extends React.Component<AnsweredQuestionProps> {

  constructor(props: AnsweredQuestionProps) {
    super(props);
  }

  render() {
    return (
      <div className="AnsweredCards" >
        {this.getAnsweredCards(this.props.questions)}
      </div>
    );
  }

  private getAnsweredCards(questions: AnsweredQuestion[]) {
    if (questions.length > 12) {
      questions = questions.splice(12, questions.length - 12);
    }
    return questions.map((q, i) => (
      <AnsweredQuestionCardComponent key={questions.length - i} question={q} />
    ));
  }

}

class AnsweredQuestionProps {
  questions: AnsweredQuestion[] = [];
}

export default AnsweredQuestionsComponent;