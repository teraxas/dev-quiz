import * as React from 'react';
import { TextField, FlatButton, Dialog } from 'material-ui';

export default class UserNamingDialog extends React.Component<UserNamingDialogProps> {
  state = {
    open: true,
    userName: null
  };

  render() {
    const actions = [
      (
        <FlatButton
          key={0}
          label="Ok"
          primary={true}
          keyboardFocused={true}
          onClick={() => this.handleClose()}
          disabled={!this.state.userName}
        />
      )
    ];

    return (
      <div>
        <Dialog
          title="[Dev].name === null :("
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          Enter your name:
          <TextField hintText="Your name" onChange={(e) => this.handleChange(e)} />
        </Dialog>
      </div>
    );
  }

  // tslint:disable-next-line:no-any
  private handleChange(event: any) {
    this.setState({
      userName: event.target.value,
    });
  }

  private handleOpen() {
    this.setState({ open: true });
  }

  private handleClose() {
    this.setState({ open: false });
    this.props.onNameSubmit(this.state.userName);
  }

}

class UserNamingDialogProps {
  onNameSubmit: Function;
}