import * as React from 'react';
import { TextField, FlatButton, Dialog, Checkbox } from 'material-ui';
import { NewQuestion } from '../model/Question';

// tslint:disable-next-line:no-any
// tslint:disable-next-line:max-line-length
export default class QuestionCreateDialog extends React.Component<QuestionCreateDialogParams, QuestionCreateDialogState> {

  constructor(props: QuestionCreateDialogParams) {
    super(props);
    this.state = new QuestionCreateDialogState();
  }

  render() {
    const actions = [
      (
        <FlatButton
          key={0}
          label="Ok"
          primary={true}
          keyboardFocused={true}
          onClick={() => this.handleOK()}
          disabled={!this.state.description || !this.state.explanation}
        />
      )
    ];

    return (
      <Dialog
        title="[Dev].canNowCreateAQuestion"
        actions={actions}
        modal={true}
        open={true}
      >
        Enter your question:
        <TextField hintText="Question" onChange={(e) => this.handleChangeQuestion(e)}  />
        <br/>
        Enter explanation:
        <TextField hintText="Explanation" onChange={(e) => this.handleChangeExplanation(e)} />
        <Checkbox
          label="Is this true?"
          checked={this.state.isTrue}
          onCheck={() => (this.setState({ isTrue: !this.state.isTrue }))}
        />
      </Dialog>
    );
  }

  private handleOK() {
    this.props.onSubmit({
      description: this.state.description,
      explanation: this.state.explanation,
      isTrue: this.state.isTrue
    });
    this.setState(new QuestionCreateDialogState());
  }

  // tslint:disable-next-line:no-any
  private handleChangeQuestion(event: any) {
    this.setState({
      description: event.target.value
    });
  }

  // tslint:disable-next-line:no-any
  private handleChangeExplanation(event: any) {
    this.setState({
      explanation: event.target.value
    });
  }

}

class QuestionCreateDialogState implements NewQuestion {
  description: string;
  explanation: string;
  isTrue: boolean;
}

class QuestionCreateDialogParams {
  onSubmit: Function;
}