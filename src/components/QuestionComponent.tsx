import * as React from 'react';
import { Card, CardText, CardActions, FlatButton, CardHeader } from 'material-ui';
import './QuestionComponent.css';
import { Question } from '../model/Question';

export class QuestionComponent extends React.Component<QuestionComponentProps> {

  constructor(props: QuestionComponentProps) {
    super(props);
  }

  render() {
    return (
      <Card
        className="QuestionCard"
      >
        <CardHeader
          title={this.props.description}
        />
        <CardActions >
          <FlatButton label="True" onClick={() => this.answer(true)} />
          <FlatButton label="False" onClick={() => this.answer(false)} />
        </CardActions>
      </Card>
    );
  }

  private answer(response: boolean) {
    this.props.onAnswer(response);
  }

}

class QuestionComponentProps implements Question {
  id: number;
  description: string;
  onAnswer: Function;
}

export default QuestionComponent;