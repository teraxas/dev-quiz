export interface User {
  uuid: number;
  name: string;
}

export interface UserResults extends User {
  correctAnswers?: number;
  totalAnswers?: number;
  canAddQuestion?: boolean;
}
