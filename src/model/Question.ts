export interface Question {
  id: number;
  description: string;
}

export interface QuestionResponse {
  id: number;
  response: boolean;
  userUUID?: string;
}

export interface QuestionResult {
  id: number;
  result: boolean;
  explanation: string;
}

export interface AnsweredQuestion extends Question, QuestionResponse, QuestionResult {
  canAddQuestion: boolean;
}

export interface NewQuestion {
  description: string;
  explanation: string;
  isTrue: boolean;
  creator?: string;
}
