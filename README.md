# DEV QUIZ React app

A small react quiz app.

## Deployment

App can be deployed by a push to heroku git repository.
This happens automatically with the help of Bitbucket Pipelines, on push to `master` branch.

## Docker

Docker is useful for local development and easier deployment environemnt setup.
Did configs. Planned to use them at first, then decided not to.
Left them be - som day they might be needed.

* DEV Docker run
`docker run -it -p 3000:3000 -v [put your path here]/frontend/src:/frontend/src [image id]`
* PROD Docker build
`docker build ./ --build-arg app_env=production`
* REOD Docker run
`docker run -i -t -p 3000:3000 [image id]`